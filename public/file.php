<?php
  header('Content-Type: text/html; charset=UTF-8');
  setlocale(LC_ALL, 'Russian_Russia.65001');

  try{
    $check = true;
    if (empty($_POST['name'])) {
        print("Заполните поле с именем.<br/>");
        $check = false;
    } if (empty($_POST['email'])) {
        print("Заполните поле с почтой.<br/>");
        $check = false;
    } if (empty($_POST['birthday'])) {
        print("Заполните поле с датой рождения.<br/>");
        $check = false;
    } if (empty($_POST['gender'])) {
        print("Укажите ваш пол, нажав на радиокнопку.<br/>");
        $check = false;
    } if (empty($_POST['body_parts'])) {
        print("Укажите кол-во ваших конечностей с помощью радиокнопки.<br/>");
        $check = false;
    } if (empty($_POST['superpower'])) {
        print("Вы должны указать ваши сверхспособности.<br/>");
        $check = false;
    } if (empty($_POST['Biography'])) {
        print("Заполните поле с биографией.<br/>");
        $check = false;
    } if (empty($_POST['terms'])) {
        print("Ознакомьтесь с контрактом, чтобы отправить форму.<br/>");
        $check = false;
    }
    if ($check==false) exit;

    $information = new PDO("mysql:host='localhost'; dbname='u20961'", 'u20961', '8380532', array(PDO::ATTR_PERSISTENT => true));

    $people = $information->prepare("INSERT INTO people SET name = ?, email = ?, birthday = ?, gender = ?, body_parts = ?, Biography = ?");
    $people -> execute([$_POST['name'], $_POST['email'], $_POST['birthday'], $_POST['gender'], $_POST['body_parts'], $_POST['Biography']]);
    $id_person = $information->lastInsertId();

    $list = implode(', ',$_POST['superpower']);
    $superpower = $information->prepare("INSERT INTO superpower SET list = ?");
    $superpower -> execute([$list]);
    $id_superpower = $information->lastInsertId();

    $person_with_superpowers = $information->prepare("INSERT INTO person_with_superpowers SET id_person = ?, id_superpower = ?");
    $person_with_superpowers -> execute([$id_person, $id_superpower]);

    echo "Запрос отправлен!";
  } catch(PDOException $e){
    echo "Oшибка при отправке запроса!";
    print('Error : ' . $e->getMessage());
  }
?>