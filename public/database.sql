CREATE TABLE people (
  id_person int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name varchar(32) NOT NULL,
  email varchar(64) NOT NULL,
  birthday int(4) NOT NULL,
  gender varchar(5) NOT NULL,
  boby_parts char(1) NOT NULL,
  Biography varchar(128)
);

CREATE TABLE superpower (
  id_superpower int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  list varchar(64) NOT NULL
);

CREATE TABLE person_with_superpowers (
  id_person int(10) NOT NULL,
  id_superpower int(10) NOT NULL
);